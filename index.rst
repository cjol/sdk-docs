.. Manner Node SDK documentation master file, created by
   sphinx-quickstart on Tue Jan 17 09:38:53 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

API Documentation
=================

We currently offer a generic API for any conversational interface, and have
native Facebook Messenger and Botkit integrations coming soon. It is highly recommended
that you use an SDK if one is available -- please `request one <mailto:christopher@manner.ai>`_
if none is yet available for your preferred language.

.. toctree::
	:maxdepth: 1
	:caption: Integration Guides

	node

API Key
-------

Before you begin, you will need to obtain an API key from the `Manner Dashboard <#>`_.
Every bot will need its own key for tracking.

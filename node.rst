.. Manner Node SDK documentation master file, created by
   sphinx-quickstart on Tue Jan 17 09:38:53 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Node.js Integration Guide
=========================

.. highlight:: javascript
.. role:: js(code)
   :language: javascript

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Installation
------------

You can install the Manner Node.js SDK via NPM:

.. code-block:: bash

	npm install --save manner-sdk-js

Within your project, include the Manner SDK and initialise it using your :ref:`API Key`::

	var Manner = require( "manner-sdk-js" );
	Manner.setToken( "your-api-key-goes-here" );

Basic Setup
-----------

.. When a new conversation starts, let Manner know. You can use anything in place of
.. `userId`, but it should be unique for each user of your agent::

.. var conversation = Manner.startConversation( userId );

.. TODO: This is a step that dashbot doesn't have. Do we need to distinguish between separate conversations?

Every time your agent recieves a new message, you should let Manner know::

	Manner.logIncoming( userId, messageBody );

You can use anything in place of :js:`userId`, but it should be unique for each
user of your agent. Email addresses can be a good choice. Additionally, every
time your agent decides to send a message to the user, you should again let Manner know::

	Manner.logOutgoing( userId, messageBody );

If you like, you can stop reading here! The remainder of this documentation is for
optional features. To get the most out of Manner, it is worth integrating these
features, but it is possible to get started without them.

Goal Tracking
-------------

In order to know which of your agent's messages are most effective, Manner needs
to know what goals your agent has, and how often that goal is achieved. Goals can
measure anything you like, and Manner will help you optimise towards that target.
For example, you may wish to increase the frequency that users make an order,
increase the number of conversations which last at least 10 messages, or the
number of users who return after a day.

At any point, you can start tracking progress towards a goal::

	Manner.startGoal( userId, "pizzaOrder" );

The name of the goal (:js:`"pizzaOrder"` in the example above) can again be anything you
like, as long as it is unique among goals. It will be displayed on the Manner dashboard
when viewing conversion rates. Finally, when your bot has determined that a goal
has been satisfied (e.g. just after the user has made a purchase), you can notify Manner::

	Manner.goalSuccess( userId, "pizzaOrder" );

Sometimes, goals become impossible to achieve for a particular order. To note that a
goal has failed, rather than still being pending, you can use::

	Manner.goalFail( userId, "pizzaOrder" );

You may track as many different goals as you would like to, and later choose to optimise
different parts of your conversation towards different goals.

Context
-------

To better understand the different types of user that your agent has, you can
notify Manner about any contextual information you have available. Simply add a line
to do this whenever new information is discovered. At the start of a conversation,
for example, you may wish to log the conversation channel::

	Manner.context( userId, "channel", "Slack");

After asking a particular question, you could log the answer as an additional
context point::

	Manner.context( userId, "gender", "female" );

Once again, the context type (:js:`"gender"` and :js:`"channel"` in the examples above) can be
any unique string. Within the online dashboard, you will be able to view performance
of different segments according to this type. Some context types are treated specially
by Manner for display on the main dashboard: :js:`"location"` and :js:`"channel"`.

Experimentation
---------------

The steps above will set up Manner to "passively" collect usage analytics. To
better discover which messages work best, Manner can automatically run experiments
through your agent. We distinguish between the "actions" an agent can take and the
"responses" the agent actually sends. For example, an *action* might be the decision
to send some kind of greeting to the user. The *responses* in that case could be any
of "Hello", "Hi", "Good morning!", etc. By distinguishing these concepts, we can
separate *what* your bot is doing from *how* it is doing it.

Actions
_______

To improve the "How" of your bot, you can test the different responses used
for an action. To get started, add this code at the point your bot chooses what to
say. Often this will be a string constant, or pulled from a database::

	var normalResponse = "This is the same old message I always send";
	var mannerResponse = Manner.getResponse( userId, action, normalResponse );

:js:`action` may be any unique string which represents this "node" in the conversation flow.

You should then send :js:`mannerResponse` instead of :js:`normalResponse` to your
user. For now, there will be no change to your conversations. You can add new
alternatives for this response by visiting the Manner dashboard. Once you have
done this, Manner will choose one of the given alternatives to send to each user.
Their success rates will be monitored and displayed back on the dashboard.

Tests
_____

You can use *actions* as above to determine which is the best way to say something
*once you've already decided what to say*. It is not always obvious what the best
action to take is, however. For example, it might be better to ask a user how their
day has been before you ask them to buy an item (or not!).
This is where *tests* can help. Unlike actions, tests
must be set up on the dashboard before their first use. Here, you will choose a
`slug` to refer to this Test by. Once you have created a test, you can use it
for your agent in two ways.

The simplest way is to let the Test resolve directly
into a response. In this case, an action will be chosen from the test's options,
and a response will be chosen from this action. You can  then send this response
to the user as normal::

	var mannerResponse = Manner.getTestResponse( userId, 'testSlug' );

It is also possible to choose an action from the test without resolving it to a
response. This may be useful if you need to run different code depending on the action.
You can then provide a response of your own, or use Manner to choose one::

	var mannerAction = Manner.getTestAction( userId, 'testSlug' );
	switch (mannerAction) {
		case "action1":
			...
	}
	var mannerResponse = Manner.getResponse( userId, mannerAction );

Help
----

It should be easy to get started with Manner. If you come across any difficulties,
please `let us know <mailto:christopher@manner.ai>`_ so that we can make Manner easier to use.
